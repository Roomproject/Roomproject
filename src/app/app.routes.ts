import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { SubnavComponent } from './subnav/subnav.component';

export const routes: Routes = [
    {pathMatch: 'full',
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'main',
        component: MainComponent
    },
    {
        path: 'nav',
        component: SubnavComponent
    },
]

export const routing = RouterModule.forRoot(routes);